This repo is created to demonstrate the git merge function, uses and how to
resolve if any merge conflicts.
This repo also consists a HTML file which shows the number of occurence of a 
word in a string using JavaScript.
It consists three branches, namely :
i) MASTER branch,
ii) HTML branch (where HTML code is developed and merged),
iii) JavaScript branch (which pulls the HTML code and develops the JavaScript 
     code).
